<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Group::class, 6)->create()->each(function ($contact) {
        $contact->contacts()->saveMany(factory(App\Contact::class, 10)->make());
      });
    }
}
