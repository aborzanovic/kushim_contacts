<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Group::class, function (Faker\Generator $faker) {
    static $index = -1;

    static $names = array(
      "Family", "Work", "Friends", "Random", "Other", "Following"
    );

    $index++;

    return [
        'name' => $names[$index],
    ];
});

$factory->define(App\Contact::class, function (Faker\Generator $faker) {
    $names = array(
      "Jack", "Jane", "Joe", "Mike", "Edward", "Josh", "Mark", "Keith"
    );
    $last_names = array(
      "Doe", "Malkovich", "South", "Goodman", "Goldman", "Tesla", "Greenberg"
    );
    $addresses = array(
      "First Street", "Second Street", "Third Street", "Fourth Street",
    );
    $cities = array(
      "London", "Paris", "New York", "Belgrade",
    );
    $zip = array(
      "1185", "0541", "16403", "11000", "16840"
    );
    $countries = array(
      "England", "France", "USA", "Serbia",
    );
    $phones = array(
      "03416841", "1035102534", "13510214768", "0354643101"
    );

    $fn = $names[array_rand($names)];
    $ln = $last_names[array_rand($last_names)];
    return [
      'first_name' => $fn,
      'last_name' => $ln,
      'address' => $addresses[array_rand($addresses)],
      'city' => $cities[array_rand($cities)],
      'zip' => $zip[array_rand($zip)],
      'country' => $countries[array_rand($countries)],
      'email' => "$fn.$ln@gmail.com",
      'phone' => $phones[array_rand($phones)],
      'note' => '',
      'avatar' => ''
    ];
});
