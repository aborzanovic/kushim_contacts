<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Contact;
use \App\Group;

class GroupController extends Controller
{
  public function getGroupsNum(){
    $num = Group::all()->count();
    return array('num' => $num);
  }

  public function getGroups($page, $perPage = 20){
    $groups = Group::take($perPage)->skip(($page - 1) * $perPage)->get();
    return $groups;
  }

  public function getGroup($id){
    $group = Group::with('contacts')->find($id);
    return $group;
  }

  public function insert(Request $request){
    $group = new Group;

    $group->name = $request->input('name');
    $group->contacts()->sync($request->input('contacts'));

    $group->save();

    return 'ok';
  }

  public function update(Request $request){
    $group = Group::find($request->input('id'));

    $group->name = $request->input('name');
    $group->contacts()->sync($request->input('contacts'));

    $group->save();

    return "ok";
  }

  public function delete(Request $request){
    $id = $request->input('id');

    $group = Group::find($id);

    $group->delete();

    return "ok";
  }
}
