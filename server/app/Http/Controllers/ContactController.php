<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Contact;
use Illuminate\Support\Facades\Storage;

class ContactController extends Controller
{
  public function getContactsNum(){
    $num = Contact::all()->count();
    return array('num' => $num);
  }

  public function getContacts($page, $perPage = 20){
    $contacts = Contact::take($perPage)->skip(($page - 1) * $perPage)->get();
    return $contacts;
  }

  public function getContact($id){
    $contact = Contact::find($id);
    return $contact;
  }

  public function searchContacts($name){
    $contact = Contact::where('first_name', 'LIKE', "%$name%")->orWhere('last_name', 'LIKE', "%$name%")->get();
    return $contact;
  }

  public function insert(Request $request){
    $contact = new Contact;

    $contact->first_name = $request->input('first_name');
    $contact->last_name = $request->input('last_name');
    $contact->address = $request->input('address');
    $contact->city = $request->input('city');
    $contact->zip = $request->input('zip');
    $contact->country = $request->input('country');
    $contact->email = $request->input('email');
    $contact->phone = $request->input('phone');
    $contact->note = $request->input('note');

    $path = $request->avatar->store('public/avatars');
    $contact->avatar = 'server/public' . Storage::url($path);

    $contact->save();

    return 'ok';
  }

  public function update(Request $request){
    $contact = Contact::find($request->input('id'));

    $contact->first_name = $request->input('first_name');
    $contact->last_name = $request->input('last_name');
    $contact->address = $request->input('address');
    $contact->city = $request->input('city');
    $contact->zip = $request->input('zip');
    $contact->country = $request->input('country');
    $contact->email = $request->input('email');
    $contact->phone = $request->input('phone');
    $contact->note = $request->input('note');

    if(!empty($contact->avatar)){
      Storage::delete($contact->avatar);
    }

    $path = $request->avatar->store('public/avatars');
    $contact->avatar = 'server/public' . Storage::url($path);

    $contact->save();

    return "ok";
  }

  public function delete(Request $request){
    $id = $request->input('id');

    $contact = Contact::find($id);

    $contact->delete();

    return "ok";
  }
}
