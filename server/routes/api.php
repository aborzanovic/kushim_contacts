<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// contact

Route::get('/contact-num', 'ContactController@getContactsNum');

Route::get('/contact/{page}/{perPage}', 'ContactController@getContacts');

Route::get('/contacts/search/{name}', 'ContactController@searchContacts');

Route::get('/contact/{id}', 'ContactController@getContact');

Route::post('/contact/insert', 'ContactController@insert');

Route::post('/contact/update', 'ContactController@update');

Route::post('/contact/delete', 'ContactController@delete');

//group

Route::get('/group-num', 'GroupController@getGroupsNum');

Route::get('/group/{page}/{perPage}', 'GroupController@getGroups');

Route::get('/group/{id}', 'GroupController@getGroup');

Route::post('/group/insert', 'GroupController@insert');

Route::post('/group/update', 'GroupController@update');

Route::post('/group/delete', 'GroupController@delete');
