install prerequisites via npm (in the root dir)
and via composer.phar (in the /server dir)

the laravel api backend is in the /server folder

init the laravel app: "php artisan migrate"
                      "php artisan seed --class=ContactsTableSeeder"
                      (execute these from the /server folder)

run the app: "npm run start" (from the root folder)


be sure to set up a local (virtual) server "dev.kushim.contacts"
and to set up the database config in the laravel .env file

in order for the images to work, a symlink is necessary (php artisan storage:link)