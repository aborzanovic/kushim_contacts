export class Contact {
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public address: string,
    public city: string,
    public zip: string,
    public country: string,
    public email: string,
    public phone: string,
    public note: string,
    public avatar: string
  ){}
}
