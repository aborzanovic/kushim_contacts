import { NgModule }               from '@angular/core';
import { BrowserModule }          from '@angular/platform-browser';
import { HttpModule }             from '@angular/http';
import { RouterModule, Routes }   from '@angular/router';
import { FormsModule }   from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent }           from './app.component';
import { ContactDetailsComponent } from './components/contact-details.component';
import { ContactListComponent } from './components/contact-list.component';
import { ContactFormComponent } from './components/contact-form.component';
import { GroupListComponent } from './components/group-list.component';
import { GroupFormComponent } from './components/group-form.component';
import { GroupDetailsComponent } from './components/group-details.component';

const appRoutes: Routes = [
  { path: 'contact/:id',      component: ContactDetailsComponent },
  { path: 'contacts',      component: ContactListComponent },
  { path: 'group/:id',      component: GroupDetailsComponent },
  { path: 'groups',      component: GroupListComponent }
];

@NgModule({
  imports:      [ RouterModule.forRoot(appRoutes), BrowserModule, HttpModule, FormsModule ],
  declarations: [ AppComponent, ContactDetailsComponent, ContactListComponent, GroupListComponent, ContactFormComponent, GroupFormComponent, GroupDetailsComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
