import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GroupService } from '../services/group.service';
import { Group } from '../models/group.model';
import { Contact } from '../models/contact.model';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'group-details',
  templateUrl: 'app/templates/group-details.component.html',
  //styleUrls: ['app/styles/group-details.component.css'],
  providers: [ GroupService ]
})
export class GroupDetailsComponent implements OnInit {
  private errorMessage: string;
  private group: Group;
  private showForm = false;
  private inputGroup: Group;
  private id: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private groupService: GroupService
    ){
    this.group = new Group(0, "", []);
    this.inputGroup = new Group(0,"",[]);
    this.id = 0;
  }

  reloadGroup(): void {
    this.groupService.getGroup(this.id)
        .subscribe(
              (group: Group) => this.group = group,
              error =>  this.errorMessage = <any>error
          );
  }

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.groupService.getGroup(+params['id']))
      .subscribe(
                (group: Group) => {this.group = group; this.id = group.id;},
                error =>  this.errorMessage = <any>error
                );
  }

  edit(obj: any): void {
    this.groupService.update(obj.group)
      .subscribe(
            (res: string) => this.reloadGroup(),
            error =>  this.errorMessage = <any>error
            );
  }

  delete(id: number): void {
    this.groupService.delete(id)
    .subscribe(
      (res: string) => console.log(res),
      error =>  this.errorMessage = <any>error
    );
    this.router.navigate(['/groups']);
  }

  toggleUpdateForm(): void {
    this.showForm = !this.showForm;
    var newContacts: Contact[] = [];
    this.group.contacts.forEach(val => newContacts.push(val));
    this.inputGroup = new Group(
      this.group.id,
      this.group.name,
      newContacts
    );
  }
}
