import { Component, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GroupService } from '../services/group.service';
import { Group } from '../models/group.model';

import { ContactService } from '../services/contact.service';
import { Contact } from '../models/contact.model';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'group-form',
  templateUrl: 'app/templates/group-form.component.html',
  providers: [ ContactService ]
})
export class GroupFormComponent {
  private errorMessage: string;

  @Input() group: Group;
  @Output() formSubmitted = new EventEmitter();

  searchContacts: Contact[];

  constructor(private contactService: ContactService){
    this.searchContacts = [];
  }

  submit(): void{
    this.formSubmitted.emit({
      "group": this.group
    });
  }

  searchForContacts(event: any): void {
    this.contactService.getContactsByName(event.target.value)
    .subscribe(
          (contacts: Contact[]) => this.searchContacts = contacts,
          error =>  this.errorMessage = <any>error
          );
  }

  addContact(contact: Contact): void {
    this.group.contacts.push(contact);
  }

  kickContact(id: number): void {
    var index = this.group.contacts.findIndex(el => el.id == id);

    if(index > -1){
      this.group.contacts.splice(index, 1);
    }
  }
}
