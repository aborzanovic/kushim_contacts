import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GroupService } from '../services/group.service';
import { Group } from '../models/group.model';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'group-list',
  templateUrl: 'app/templates/group-list.component.html',
  providers: [ GroupService ]
})
export class GroupListComponent implements OnInit {
  private errorMessage: string;
  private groups: Group[];
  private inputGroup: Group;
  private page: number = 1;
  private groupNum: number = 0;
  private perPage: number = 5;
  private showForm = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private groupService: GroupService
    ){
    this.groups = [];
    this.inputGroup = new Group(0,"",[]);
  }

  reloadList(): void{
    this.groupService.getGroupsNum()
    .subscribe(
        (num: number) => this.groupNum = num,
        error =>  this.errorMessage = <any>error
    );
    this.groupService.getGroups(this.page, this.perPage)
        .subscribe(
              (groups: Group[]) => this.groups = groups,
              error =>  this.errorMessage = <any>error
              );
  }

  ngOnInit(): void {
      this.reloadList();
  }

  prevPage(): void {
    if(this.page > 1){
      this.page -= 1;
      this.reloadList();
    }
  }

  nextPage(): void {
    let maxPages = Math.floor(this.groupNum / this.perPage);
    if(this.groupNum % this.perPage > 0) maxPages++;
    if(this.page < maxPages){
      this.page += 1;
      this.reloadList();
    }
  }

  new(obj: any): void {
    this.groupService.insert(obj.group)
      .subscribe(
            (res: string) => console.log(res),
            error =>  this.errorMessage = <any>error
            );
    this.reloadList();
  }
}
