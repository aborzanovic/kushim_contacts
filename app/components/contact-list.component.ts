import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ContactService } from '../services/contact.service';
import { Contact } from '../models/contact.model';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'contact-list',
  templateUrl: 'app/templates/contact-list.component.html',
  providers: [ ContactService ]
})
export class ContactListComponent implements OnInit {
  private errorMessage: string;
  private contacts: Contact[];
  private inputContact: Contact;
  private page: number = 1;
  private contactNum: number = 0;
  private perPage: number = 5;
  private showForm = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService
    ){
    this.contacts = [];
    this.inputContact = new Contact(0,"","","","","","","","","","");
  }

  reloadList(): void{
    this.contactService.getContactsNum()
    .subscribe(
        (num: number) => this.contactNum = num,
        error =>  this.errorMessage = <any>error
    );
    this.contactService.getContacts(this.page, this.perPage)
        .subscribe(
              (contacts: Contact[]) => this.contacts = contacts,
              error =>  this.errorMessage = <any>error
              );
  }

  ngOnInit(): void {
      this.reloadList();
  }

  prevPage(): void {
    if(this.page > 1){
      this.page -= 1;
      this.reloadList();
    }
  }

  nextPage(): void {
    let maxPages = Math.floor(this.contactNum / this.perPage);
    if(this.contactNum % this.perPage > 0) maxPages++;
    if(this.page < maxPages){
      this.page += 1;
      this.reloadList();
    }
  }

  new(obj: any): void {
    this.contactService.insert(obj.contact, obj.file)
      .subscribe(
            (res: string) => console.log(res),
            error =>  this.errorMessage = <any>error
            );
    this.reloadList();
  }
}
