import { Component, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import {FILE_UPLOAD_DIRECTIVES, FileUploader} from 'ng2-file-upload';

import { ContactService } from '../services/contact.service';
import { Contact } from '../models/contact.model';

import 'rxjs/add/operator/switchMap';

const root: string = 'http://dev.kushim.contacts/server/api/';

@Component({
  selector: 'contact-form',
  templateUrl: 'app/templates/contact-form.component.html'/*,
  directives: [FILE_UPLOAD_DIRECTIVES]*/
})
export class ContactFormComponent {
  @ViewChild('fileInput') fileInput: any;

  @Input() contact: Contact;
  @Output() formSubmitted = new EventEmitter();

  submit(): void{
    let fi = this.fileInput.nativeElement;
    let fileToUpload: any = null;
    if (fi.files && fi.files[0]) {
      fileToUpload = fi.files[0];
    }

    this.formSubmitted.emit({
      "contact": this.contact,
      "file": fileToUpload
    });
  }
}
