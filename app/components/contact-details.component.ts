import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ContactService } from '../services/contact.service';
import { Contact } from '../models/contact.model';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'contact-details',
  templateUrl: 'app/templates/contact-details.component.html',
  styleUrls: ['app/styles/contact-details.component.css'],
  providers: [ ContactService ]
})
export class ContactDetailsComponent implements OnInit {
  private errorMessage: string;
  private contact: Contact;
  private showForm = false;
  private inputContact: Contact;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService
    ){
    this.contact = new Contact(0, "", "", "", "", "", "", "", "", "", "");
    this.inputContact = new Contact(0,"","","","","","","","","","");
  }

  reloadContact(): void {
    this.route.params
      .switchMap((params: Params) => this.contactService.getContact(+params['id']))
      .subscribe(
                (contact: Contact) => {this.contact = contact; console.log("got a contact");},
                error =>  this.errorMessage = <any>error
                );
  }

  ngOnInit(): void {
    this.reloadContact();
  }

  edit(obj: any): void {
    this.contactService.update(obj.contact, obj.file)
      .subscribe(
            (res: string) => this.reloadContact(),
            error =>  this.errorMessage = <any>error
            );
  }

  delete(id: number): void {
    this.contactService.delete(id)
    .subscribe(
      (res: string) => console.log(res),
      error =>  this.errorMessage = <any>error
    );
    this.router.navigate(['/contacts']);
  }

  toggleUpdateForm(): void {
    this.showForm = !this.showForm;
    this.inputContact = new Contact(
      this.contact.id,
      this.contact.firstName,
      this.contact.lastName,
      this.contact.address,
      this.contact.city,
      this.contact.zip,
      this.contact.country,
      this.contact.email,
      this.contact.phone,
      this.contact.note,
      this.contact.avatar
    );
  }
}
