import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Contact } from '../models/contact.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';

@Injectable()
export class ContactService {
  private root: string = 'http://dev.kushim.contacts/server/api/';

  constructor(private http: Http){}

  getContact(id: number): Observable<Contact> {
    return this.http.get(this.root + 'contact/' + id)
                    .first()
                    .map(this.extractContact)
                    .catch(this.handleError);
  }

  getContacts(page: number, perPage = 20): Observable<Contact[]> {
    return this.http.get(this.root + 'contact/' + page + '/' + perPage)
                    .first()
                    .map(this.extractContacts)
                    .catch(this.handleError);
  }

  getContactsByName(name: string): Observable<Contact[]> {
    return this.http.get(this.root + 'contacts/search/' + name)
                    .first()
                    .map(this.extractContacts)
                    .catch(this.handleError);
  }

  getContactsNum(): Observable<number> {
    return this.http.get(this.root + 'contact-num')
                    .first()
                    .map(res => res.json().num)
                    .catch(this.handleError);
  }

  insert(contact: Contact, avatar: any): Observable<string> {
    var input = new FormData();
    input.append('id', contact.id);
    input.append('first_name', contact.firstName);
    input.append('last_name', contact.lastName);
    input.append('address', contact.address);
    input.append('city', contact.city);
    input.append('zip', contact.zip);
    input.append('country', contact.country);
    input.append('email', contact.email);
    input.append('phone', contact.phone);
    input.append('note', contact.note);
    input.append('avatar', avatar);

    return this.http.post(this.root + 'contact/insert', input)
                    .first()
                    .catch(this.handleError);
  }

  update(contact: Contact, avatar: any): Observable<string> {
    var input = new FormData();
    input.append('id', contact.id);
    input.append('first_name', contact.firstName);
    input.append('last_name', contact.lastName);
    input.append('address', contact.address);
    input.append('city', contact.city);
    input.append('zip', contact.zip);
    input.append('country', contact.country);
    input.append('email', contact.email);
    input.append('phone', contact.phone);
    input.append('note', contact.note);
    input.append('avatar', avatar);

    return this.http.post(this.root + 'contact/update', input)
                    .first()
                    .catch(this.handleError);
  }

  delete(id: number): Observable<string> {
    let headers      = new Headers({ 'Content-Type': 'application/json' });
    let options       = new RequestOptions({ headers: headers });
    return this.http.post(this.root + 'contact/delete', {'id' : id}, options)
                    .first()
                    .catch(this.handleError);
  }

  private extractContact(res: Response): Contact {
    let json = res.json();
    let contact = new Contact(
      json.id,
      json.first_name,
      json.last_name,
      json.address,
      json.city,
      json.zip,
      json.country,
      json.email,
      json.phone,
      json.note,
      json.avatar
    );

    return contact;
  }

  private extractContacts(res: Response): Contact[] {
    let json = res.json();
    let contacts: Contact[] = [];
    json.forEach(function(body: any){
      let contact = new Contact(
        body.id,
        body.first_name,
        body.last_name,
        body.address,
        body.city,
        body.zip,
        body.country,
        body.email,
        body.phone,
        body.note,
        body.avatar
      );

      contacts.push(contact);
    });

    return contacts;
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
