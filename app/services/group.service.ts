import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Group } from '../models/group.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';

@Injectable()
export class GroupService {
  private root: string = 'http://dev.kushim.contacts/server/api/';

  constructor(private http: Http){}

  getGroup(id: number): Observable<Group> {
    return this.http.get(this.root + 'group/' + id)
                    .first()
                    .map(this.extractGroup)
                    .catch(this.handleError);
  }

  getGroups(page: number, perPage = 20): Observable<Group[]> {
    return this.http.get(this.root + 'group/' + page + '/' + perPage)
                    .first()
                    .map(this.extractGroups)
                    .catch(this.handleError);
  }

  getGroupsNum(): Observable<number> {
    return this.http.get(this.root + 'group-num')
                    .first()
                    .map(res => res.json().num)
                    .catch(this.handleError);
  }

  insert(group: Group): Observable<string> {
    let headers      = new Headers({ 'Content-Type': 'application/json' });
    let options       = new RequestOptions({ headers: headers });

    return this.http.post(this.root + 'group/insert', group, options)
                    .first()
                    .catch(this.handleError);
  }

  update(group: Group): Observable<string> {
    var arr = group.contacts.map(val => val.id);

    let headers      = new Headers({ 'Content-Type': 'application/json' });
    let options       = new RequestOptions({ headers: headers });

    return this.http.post(this.root + 'group/update', {
      "id" : group.id,
      "name" : group.name,
      "contacts" : arr
    }, options)
                    .first()
                    .catch(this.handleError);
  }

  delete(id: number): Observable<string> {
    let headers      = new Headers({ 'Content-Type': 'application/json' });
    let options       = new RequestOptions({ headers: headers });
    return this.http.post(this.root + 'group/delete', {'id' : id}, options)
                    .first()
                    .catch(this.handleError);
  }

  private extractGroup(res: Response): Group {
    let json = res.json();
    if(json.contacts){
      json.contacts.forEach(function(val: any) {
        val.firstName = val.first_name;
        val.lastName = val.last_name;
      });
    }
    let group = new Group(
      json.id,
      json.name,
      json.contacts
    );

    return group;
  }

  private extractGroups(res: Response): Group[] {
    let json = res.json();
    let groups: Group[] = [];
    json.forEach(function(body: any){
      if(json.contacts){
        body.contacts.forEach(function(val: any) {
          val.firstName = val.first_name;
          val.lastName = val.last_name;
        });
      }
      let group = new Group(
        body.id,
        body.name,
        body.contacts
      );

      groups.push(group);
    });

    return groups;
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
